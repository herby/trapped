Smalltalk createPackage: 'Trapped-Counter'!
(Smalltalk packageAt: 'Trapped-Counter' ifAbsent: [ self error: 'Package not created: Trapped-Counter' ]) imports: {'trapped/Trapped-Processors'}!
Object subclass: #CounterApp
	instanceVariableNames: ''
	package: 'Trapped-Counter'!

!CounterApp methodsFor: 'startup'!

start
	| viewModel |
	viewModel := TrappedCounter new.
	viewModel axxord: SimpleAxon new.
	Trapped start: { viewModel }
! !

!CounterApp class methodsFor: 'startup'!

start
	^ self new start
! !

Object subclass: #TrappedCounter
	instanceVariableNames: 'value'
	package: 'Trapped-Counter'!

!TrappedCounter methodsFor: 'accessing'!

value
	^value
!

value: aNumber
	value := aNumber
! !

!TrappedCounter methodsFor: 'action'!

decrement
	value := value - 1
!

increment
	value := value + 1
! !

!TrappedCounter methodsFor: 'initialization'!

initialize
	super initialize.
	value := 0
! !

