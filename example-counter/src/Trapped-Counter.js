define(["amber/boot"
//>>excludeStart("imports", pragmas.excludeImports);
, "trapped/Trapped-Processors"
//>>excludeEnd("imports");
, "amber_core/Kernel-Objects"], function($boot
//>>excludeStart("imports", pragmas.excludeImports);

//>>excludeEnd("imports");
){"use strict";
if(!("nilAsValue" in $boot))$boot.nilAsValue=$boot.nilAsReceiver;
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
$core.addPackage("Trapped-Counter");
($core.packageDescriptors||$core.packages)["Trapped-Counter"].innerEval = function (expr) { return eval(expr); };
($core.packageDescriptors||$core.packages)["Trapped-Counter"].imports = ["trapped/Trapped-Processors"];
($core.packageDescriptors||$core.packages)["Trapped-Counter"].transport = {"type":"amd","amdNamespace":"trapped-counter"};

$core.addClass("CounterApp", $globals.Object, [], "Trapped-Counter");
$core.addMethod(
$core.method({
selector: "start",
protocol: "startup",
fn: function (){
var self=this,$self=this;
var viewModel;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
viewModel=$recv($globals.TrappedCounter)._new();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["new"]=1;
//>>excludeEnd("ctx");
$recv(viewModel)._axxord_($recv($globals.SimpleAxon)._new());
$recv($globals.Trapped)._start_([viewModel]);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"start",{viewModel:viewModel},$globals.CounterApp)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "start\x0a\x09| viewModel |\x0a\x09viewModel := TrappedCounter new.\x0a\x09viewModel axxord: SimpleAxon new.\x0a\x09Trapped start: { viewModel }",
referencedClasses: ["TrappedCounter", "SimpleAxon", "Trapped"],
//>>excludeEnd("ide");
messageSends: ["new", "axxord:", "start:"]
}),
$globals.CounterApp);


$core.addMethod(
$core.method({
selector: "start",
protocol: "startup",
fn: function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._new())._start();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"start",{},$globals.CounterApp.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "start\x0a\x09^ self new start",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["start", "new"]
}),
$globals.CounterApp.a$cls);


$core.addClass("TrappedCounter", $globals.Object, ["value"], "Trapped-Counter");
$core.addMethod(
$core.method({
selector: "decrement",
protocol: "action",
fn: function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self["@value"]=$recv($self["@value"]).__minus((1));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"decrement",{},$globals.TrappedCounter)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "decrement\x0a\x09value := value - 1",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["-"]
}),
$globals.TrappedCounter);

$core.addMethod(
$core.method({
selector: "increment",
protocol: "action",
fn: function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self["@value"]=$recv($self["@value"]).__plus((1));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"increment",{},$globals.TrappedCounter)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "increment\x0a\x09value := value + 1",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["+"]
}),
$globals.TrappedCounter);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
fn: function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($globals.TrappedCounter.superclass||$boot.nilAsClass).fn.prototype._initialize.apply($self, []));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = false;
//>>excludeEnd("ctx");;
$self["@value"]=(0);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{},$globals.TrappedCounter)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x09value := 0",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["initialize"]
}),
$globals.TrappedCounter);

$core.addMethod(
$core.method({
selector: "value",
protocol: "accessing",
fn: function (){
var self=this,$self=this;
return $self["@value"];

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "value\x0a\x09^value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.TrappedCounter);

$core.addMethod(
$core.method({
selector: "value:",
protocol: "accessing",
fn: function (aNumber){
var self=this,$self=this;
$self["@value"]=aNumber;
return self;

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aNumber"],
source: "value: aNumber\x0a\x09value := aNumber",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.TrappedCounter);


});
