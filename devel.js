define([
    './testing',
    'amber/devel',
    // --- packages used only during development begin here ---
    'axxord/Axxord-Tests',
    'amber/legacy/IDE'
    // --- packages used only during development end here ---
], function (amber) {
    return amber;
});
