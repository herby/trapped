Smalltalk createPackage: 'Trapped-Todo'!
(Smalltalk packageAt: 'Trapped-Todo' ifAbsent: [ self error: 'Package not created: Trapped-Todo' ]) imports: {'trapped/Trapped-Processors'}!
Object subclass: #TodoApp
	instanceVariableNames: ''
	package: 'Trapped-Todo'!

!TodoApp methodsFor: 'startup'!

start
	| viewModel axon |
	viewModel := Axolator on: TrappedTodo new.
    axon := SimpleAxon new.
	
	axon addInterest: (TrappedPosition
		interestOn: #((todos) nil)
		block: [ axon changed: #((remaining)) ]).

    [ viewModel axes: #((todos)) transform: [{
        #{'text'->'learn trapped'. 'done'->true}.
        #{'text'->'build a trapped app'. 'done'->false}
    }]] valueWithTimeout: 2000.

	viewModel axxord: axon.
	Trapped start: { viewModel }
! !

!TodoApp class methodsFor: 'startup'!

start
	^ self new start
! !

Object subclass: #TrappedTodo
	instanceVariableNames: 'title todos todoText'
	package: 'Trapped-Todo'!
!TrappedTodo commentStamp!
// Code from AngularJS Todo example, http://angularjs.org/#todo-js
function TodoCtrl($scope) {
  $scope.todos = [
    {text:'learn angular', done:true},
    {text:'build an angular app', done:false}];
 
  $scope.addTodo = function() {
    $scope.todos.push({text:$scope.todoText, done:false});
    $scope.todoText = '';
  };
 
  $scope.remaining = function() {
    var count = 0;
    angular.forEach($scope.todos, function(todo) {
      count += todo.done ? 0 : 1;
    });
    return count;
  };
 
  $scope.archive = function() {
    var oldTodos = $scope.todos;
    $scope.todos = [];
    angular.forEach(oldTodos, function(todo) {
      if (!!todo.done) $scope.todos.push(todo);
    });
  };
}!

!TrappedTodo methodsFor: 'accessing'!

remaining
    ^self todosNotDone size
!

title
	^title
!

title: aString
	title := aString
!

todoText
	^todoText
!

todoText: aString
	todoText := aString
!

todos
	^todos
!

todos: anArray
	todos := anArray
!

todosNotDone
    ^self todos reject: [ :each | each at: 'done' ]
! !

!TrappedTodo methodsFor: 'action'!

addTodo
    self todos add: #{'text'->self todoText. 'done'->false}.
    self todoText: ''
!

archive
    self todos: self todosNotDone
! !

!TrappedTodo methodsFor: 'initialization'!

initialize
	super initialize.

	title := 'Todo'.
	todoText := nil.
	todos := nil
! !

!TrappedProcessor class methodsFor: '*Trapped-Todo'!

classDoneXxx
	"This processor is not used any more,
	it was replaced by generic
	(replace ^ with ^done-) (attr class)
	in HTML.

	This example is left here to show how you can create quick
	toView-only processor without class by just passing a block"

	^self dataToView: [ :carrier | carrier target class: 'done-', carrier value ]
! !

